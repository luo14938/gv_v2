package com.example.denni.newstest;

/**
 * Created by Denni on 23.01.2015.
 */
public interface NavigationDrawerCallbacks {
    void onNavigationDrawerItemSelected(int position);
}
