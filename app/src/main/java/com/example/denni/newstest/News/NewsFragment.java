package com.example.denni.newstest.News;


import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.example.denni.newstest.R;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.List;

/**
 * Created by Denni on 23.01.2015.
 */
public class NewsFragment extends Fragment {
    private CardArrayAdapter cardArrayAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((ActionBarActivity) getActivity()).getSupportActionBar().setTitle(R.string.news_title);
        View rootView = inflater.inflate(R.layout.fragment_news, container, false);

        ListView listView = (ListView) rootView.findViewById(R.id.card_listView);

        cardArrayAdapter = new CardArrayAdapter(getActivity(), R.layout.rss_feed_item);

        new GetFeedTask().execute();

        listView.setAdapter(cardArrayAdapter);

        return rootView;
    }

    private class GetFeedTask extends AsyncTask<Void, Void, List<Card>> {


        @Override

        protected List<Card> doInBackground(Void... params) {

            try {

                Request request = new Request.Builder()

                        .url("http://www.spiegel.de/schlagzeilen/index.rss")

                        .build();


                Response response = new OkHttpClient().newCall(request).execute();

                List<Card> items = null;

                try {

                    items = new NewsRssParser().parse(response.body().byteStream());

                } catch (XmlPullParserException e) {

                    e.printStackTrace();

                }
                return items;


            } catch (IOException e) {

                e.printStackTrace();

                return null;

            }

        }


        @Override

        protected void onPostExecute(List<Card> items) {

            if (items != null) {
                cardArrayAdapter.clear();

                cardArrayAdapter.addAll(items);
            }

        }

    }
}

