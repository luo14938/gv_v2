package com.example.denni.newstest.News;

/**
 * Created by Dennis on 29.01.2015.
 */
public class Card {
    private String line1;

    public Card(String line1) {
        this.line1 = line1;
    }

    public String getLine1() {
        return line1;
    }
}
